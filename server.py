import os
import json
from datetime import datetime
from flask import Flask, render_template, request

app = Flask(__name__)
port = int(os.getenv('PORT', 5050))


@app.route('/')
def index():
    return render_template('home.html')


@app.route('/search')
def search():
    with open('data.json') as f:
        mydata = json.load(f)

    now = datetime.now().time()
    word = request.values['word']
    if word in mydata:
        return render_template(
            'home.html', result=mydata[word], word=word, now=now)
    return render_template(
        'home.html', result='Word not found!', word=word, now=now)


@app.route('/api/search')
def search():
    with open('data.json') as f:
        mydata = json.load(f)
    
    word = request.values['word']
    if word in mydata:
        return mydata[word]
    return 'Word not found!'


if __name__ == '__main__':
    app.run(debug=True, port=port, host='0.0.0.0')
